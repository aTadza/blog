//On isole le code dans la fonction anonyme on atted la fin du chargement du DOM

$(function() {

	var menu = ['Accueil', 'Mes voyages', 'Photos', 'Contact'];
	var categories1 = ['Web Design', 'HTML', 'Freebies'];
	var categories2 = ['JavaScript', 'CSS', 'Tutorials'];

	$('#title').html('Mes petits voyages !');
	$('h1').html('À la découverte du monde <small> grâce au JS<small>');

	for (var i = 0, c = menu.length; i < c; i++) {
		$('#menu').append('<li class="nav-item"><a class="nav-link" href="#">' + menu[i] +'</a></li>');
	}

	for (var i = 0, c = categories1.length; i < c; i++) {
		$('#categories1').append('<li><a href="#">' + categories1[i] +'</a></li>');
	}
	for (var i = 0, c = categories2.length; i < c; i++) {
		$('#categories2').append('<li><a href="#">' + categories2[i] +'</a></li>');
	}
	
});